<?php

namespace Drupal\pu_video_embed_mediacentral\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\TransferException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * MediaCentral video provider plugin implementation.
 *
 * @VideoEmbedProvider(
 *   id = "mediacentral",
 *   title = @Translation("Media Central")
 * )
 */
class MediaCentral extends ProviderPluginBase {

  /**
   * A logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, $plugin_id, array $plugin_definition, ClientInterface $http_client, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $http_client);

    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('http_client'), $container->get('logger.factory')->get('pu_video_embed_mediacentral'));
  }

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    $html = $this->oEmbedData('html');
    if ($html) {
      // Ideally we would not just return the raw HTML from the oEmbed reponse
      // and would instruct construct a well formed iframe or embed code that
      // inserted the video ID. That way we don't need a server-side HTTP
      // call to an external service to render a video.
      return [
        '#type' => 'inline_template',
        '#template' => $this->oEmbedData('html'),
      ];
    }
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    // Note: Kaltura's thumbnail url uses https, which may not work locally.
    // Guzzle may be unable to get the local issuer certificate (curl error 60).
    return $this->oEmbedData('thumbnail_url');
  }

  /**
   * Get the oEmbed data for this video.
   *
   * @param string|bool $key
   *   An optional key to retrieve.
   *
   * @return bool|object
   *   An oEmbed object or FALSE on error.
   */
  protected function oEmbedData($key = FALSE) {
    $url = sprintf('https://mediacentral.princeton.edu/oembed?url=https://mediacentral.princeton.edu/id/%s/&format=json', $this->getVideoId());
    try {
      // Keep the API request timeout quite short since it needs to be called
      // everytime the video is rendered (though it's saved in render cache).
      $response = $this->httpClient->request('GET', $url, [
        'timeout' => 5,
      ]);
    }
    catch (TransferException $e) {
      $this->logger->error('Error retrieving oEmbed data at url @url: @error', ['@url' => $url, '@error' => $e->getMessage()]);
      return FALSE;
    }

    $response_data = $response->getBody()->__toString();
    try {
      $oembed = \GuzzleHttp\json_decode($response_data, TRUE);
    }
    catch (\InvalidArgumentException $e) {
      $this->logger->error('Error decoding response into JSON: @error', ['@error' => $e->getMessage()]);
      return FALSE;
    }

    if ($key) {
      if (!array_key_exists($key, $oembed)) {
        $this->logger->error('Requested oembed key "@key" does not exist.', ['@key' => $key]);
        return FALSE;
      }
      else {
        return $oembed[$key];
      }
    }
    else {
      return $oembed;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getLocalThumbnailUri() {
    return $this->thumbsDirectory . '/pu-mediacentral-' . $this->getVideoId() . '.jpg';
  }

  /**
   * {@inheritdoc}
   *
   * The width, height, and playerid parameters from the input URL are ignored.
   */
  public static function getIdFromInput($input) {
    preg_match('/https:\/\/mediacentral\.princeton\.edu\/id\/(?<id>[a-zA-Z0-9_]*)(.*?)/', $input, $matches);
    return isset($matches['id']) ? $matches['id'] : FALSE;
  }

}
