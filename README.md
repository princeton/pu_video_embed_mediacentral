CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Credits

INTRODUCTION
------------

This module provides a handler for the video embed field module. With this module 
you can add embedded videos from http://mediacentral.princeton.edu to your website. 
You must use the oEmbed share address for the video url, for example:  
https://mediacentral.princeton.edu/id/1_b024qge9  
https://mediacentral.princeton.edu/id/1_b024qge9?width=608&height=402&playerId=14292322 

The module can also get the thumbnail image.


REQUIREMENTS
============

- Video Embed Field
  https://drupal.org/project/video_embed_field

INSTALLATION
------------

1. Install and enable the module as usual.
2. If using Composer, you can run the following commands to add the repository and the
requirement to your composer.json file:  
composer config repositories vcs git@bitbucket.org:princeton/pu_video_embed_mediacentral.git  
composer require princeton/pu_video_embed_mediacentral:master
3. In the Video Embed field settings, enable Media Central as an allowed provider.


CREDITS
-------
* Michael Muzzie (notmike)
